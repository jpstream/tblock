@echo off

:: Get the directory where this file is located
set "DIRNAME=%~dp0"

:: Copy license and two scripts to prepare the build
cd "%DIRNAME%\..\.."
copy LICENSE "%DIRNAME%\LICENSE.txt"
copy "%DIRNAME%\tblock.py" run-tblock.py
copy "%DIRNAME%\tblockc.py" run-tblockc.py

:: Compile the two executables
pyinstaller -F run-tblock.py -i "%DIRNAME%\icon.ico" --paths tblock 
pyinstaller -F run-tblockc.py -i "%DIRNAME%\icon.ico" --paths tblock

:: Copy the executables to working tree
copy dist\run-tblock.exe "%DIRNAME%\tblock.exe"
copy dist\run-tblockc.exe "%DIRNAME%\tblockc.exe"

:: Enter the working tree and build the installer
cd "%DIRNAME%"
makensis setup.nsi
