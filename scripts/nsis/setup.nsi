!include "MUI2.nsh"
!include "LogicLib.nsh"
!include "WinVer.nsh"
!include "x64.nsh"

!define PRODUCT_NAME "TBlock"
!define PRODUCT_DESCRIPTION "An anti-capitalist ad-blocker that uses the hosts file"  
!define COPYRIGHT "Copyright (c) 2021 Twann"
!define PRODUCT_VERSION "0.0.6"
!define SETUP_VERSION 0.0.6

Name "TBlock"
OutFile "tblock-setup.exe"
InstallDir "$PROGRAMFILES\TBlock"
RequestExecutionLevel admin

VIProductVersion "${PRODUCT_VERSION}.1"
VIAddVersionKey "ProductName" "${PRODUCT_NAME}"
VIAddVersionKey "ProductVersion" "${PRODUCT_VERSION}"
VIAddVersionKey "FileDescription" "${PRODUCT_DESCRIPTION}"
VIAddVersionKey "LegalCopyright" "${COPYRIGHT}"
VIAddVersionKey "FileVersion" "${SETUP_VERSION}"

!define MUI_ICON "icon.ico"
!define MUI_HEADERIMAGE
;!define MUI_HEADERIMAGE_BITMAP "header.bmp"
;!define MUI_WELCOMEFINISHPAGE_BITMAP "wizard.bmp"
!define MUI_FINISHPAGE_NOAUTOCLOSE

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE "LICENSE.txt"
!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_WELCOME
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES
!insertmacro MUI_UNPAGE_FINISH

!insertmacro MUI_LANGUAGE "English"

Section "Ad-blocker"
	SetOutPath "$INSTDIR"
	File "tblock.exe"
	File "PathEd.exe"
	WriteUninstaller "$INSTDIR\uninstall-tblock.exe"
	ExecWait 'PathEd.exe add "$INSTDIR"'
SectionEnd
Section "Filter converter"
	SetOutPath "$INSTDIR"
	File "tblockc.exe"
	File "PathEd.exe"
	WriteUninstaller "$INSTDIR\uninstall-tblock.exe"
	ExecWait 'PathEd.exe add "$INSTDIR"'
SectionEnd

Section "Uninstall"
	Delete "$INSTDIR\uninstall-tblock.exe"
	Delete "$INSTDIR\tblock.exe"
	Delete "$INSTDIR\tblockc.exe"
	ExecWait '$INSTDIR\PathEd.exe remove "$INSTDIR"'
	Delete "$INSTDIR\PathEd.exe"
SectionEnd
