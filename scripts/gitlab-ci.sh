#!/bin/sh

# This script is based on the one located here: https://codeberg.org/gitnex/GitNex/src/branch/main/scripts/add-commit-status.sh
# The original script is released under the GNU General Public License, version 3 and is authored by opyale.

context="GitLab CI"
description="GitLab continuous integration tool"
state=$STATE
target_url="https://framagit.org/twann/tblock/-/pipelines"

body='
{
"context": "'$context'",
"description": "'$description'",
"state": "'$state'",
"target_url": "'$target_url'"
}
'

curl --request POST \
--data "$body" \
--header "Accept: application/json" \
--header "Content-Type: application/json" \
"${INSTANCE}/api/v1/repos/${MAIN_REPO}/statuses/${CI_COMMIT_SHA}?token=${GITEA_SECRET}"
