#!/usr/bin/env bash


# This script assumes that:

# 0. You have 'git' installed on your device (lol)
# 1. Your local git repo is up-to-date with its origin
# 2. The online repo is on a Gitea instance
# 3. You have 'tea' installed on your device and can access the online repo with it
# 4. You have 'python3', 'python3-pip', 'twine' and 'virtualenv' installed on your device
# 5. You have 'rpmbuild' installed on your device
# 6. You have access to a PyPI repository that matches the name of this project where to push new releases


# Before executing this script, you should always check:

# 1. That you wrote new version tag inside the file: scripts/package_info.sh
# 2. That the remote origin is up-to-date
# 3. That you added all files that need to be committed to commit
# 4. That you changed everything you need to change inside this current file


# Change the directory where the script is executed from
cd "$(dirname "$0")/.." || exit

# Check if the Makefile exists, exit with error code 1 if it doesn't
if ! [[ -f "Makefile" ]]; then
  echo "ERROR: cannot find Makefile"
  exit 1
fi

# Help page
if [[ "$1" == "--help" ]]; then
  echo "usage: $0 <--help|--pre-release>"
  exit
fi

# Set default variables
PRE_RELEASE=""
if [[ "$1" == "--pre-release" ]]; then
  PRE_RELEASE="--prerelease"
fi
BRANCH="$(git rev-parse --abbrev-ref HEAD)"
CHANGELOG_PATH="new_version_changelog.tmp"
EDITOR="$(git config --get core.editor)"
TEMPLATE_MSG="<!-- DO NOT EDIT THIS LINE. WRITE NEW CHANGES IN MARKDOWN FORMAT BELOW -->"
LAST_VERSION=""
CURRENT_CHANGELOG="$(cat CHANGELOG.md)"

# Load package information
source scripts/package_info.sh

# Get latest version before upgrade
for x in $(git rev-parse --abbrev-ref --tags); do
  LAST_VERSION="$x";
done

# Check that the new version is right before anything else
echo -e "The current version seems to be: $LAST_VERSION"
echo -e "The new version seems to be: $PKGVERSION"
# shellcheck disable=SC2162
read -p "Is it right ? (y/n) " -n 1
if [[ ! $REPLY =~ ^[Yy]$ ]]; then echo; exit 1; fi
echo

# Clean the whole workspace before anything else
make clean

# Re-create the virtual environment if it does not exist
make venv

# Change the version in __init__.py and in tblock.spec
sed -i "s/__version__ = '$LAST_VERSION'/__version__ = '$PKGVERSION'/" tblock/__init__.py
sed -i "s/Version:        $LAST_VERSION/Version:        $PKGVERSION/" tblock.spec
git add tblock/__init__.py tblock.spec scripts/package_info.sh

# Prepare for building
make

# Pass all tests
make test

# Build python sdist and wheel
make pypi

# Write new release notes
echo -e "$TEMPLATE_MSG" > "$CHANGELOG_PATH"
"$EDITOR" "$CHANGELOG_PATH"
sed -i "s/$TEMPLATE_MSG//" "$CHANGELOG_PATH"
NEW_CHANGES="$(cat $CHANGELOG_PATH)"
if [[ "$NEW_CHANGES" == "" ]]; then echo "ERROR: no changes specified"; exit 1; fi

# Write new release notes to CHANGELOG.md
echo -e "$NEW_CHANGES\n" > CHANGELOG.md
echo -e "$CURRENT_CHANGELOG\n" >> CHANGELOG.md
git add CHANGELOG.md

# Commit new changes
# shellcheck disable=SC2162
read -p "Ready to commit ? (y/n) " -n 1
if [[ ! $REPLY =~ ^[Yy]$ ]]; then echo; exit 1; fi
echo
git commit -S -m "release $PKGVERSION"
read -p "Ready to add new tag ? (y/n) " -n 1
if [[ ! $REPLY =~ ^[Yy]$ ]]; then echo; exit 1; fi
echo
git tag -sm "Version $PKGVERSION" "$PKGVERSION"

# Push to remote repository
# shellcheck disable=SC2162
read -p "Ready to push ? (y/n) " -n 1
if [[ ! $REPLY =~ ^[Yy]$ ]]; then echo; exit 1; fi
echo
git push origin "$BRANCH"
git push origin "$PKGVERSION"
git push gitlab "$BRANCH"
git push origin "$PKGVERSION"

# Upload new packages to PyPI
# shellcheck disable=SC2162
if [[ $PRE_RELEASE == "--prerelease" ]]; then
    read -p "Ready to upload to PyPI ? (y/n) " -n 1
    if [[ ! $REPLY =~ ^[Yy]$ ]]; then echo; exit 1; fi
    echo
    python3 -m twine upload pkg/pypi-dist/* # This will ask for credentials
fi

# Create Gitea release
# shellcheck disable=SC2162
read -p "Ready to create new Gitea release and upload packages ? (y/n) " -n 1
if [[ ! $REPLY =~ ^[Yy]$ ]]; then echo; exit 1; fi
echo
tea releases create --tag "$PKGVERSION"\
  --target "$BRANCH"\
  --title "Version $PKGVERSION"\
  --note "$NEW_CHANGES"\
  "$PRE_RELEASE"

# Clean workspace
echo -e "Cleaning workspace..."
make clean

if [[ $PRE_RELEASE == "--prerelease" ]]; then
    # Build source RPM
    read -p "Ready to build source RPM ? (y/n) " -n 1
    if [[ ! $REPLY =~ ^[Yy]$ ]]; then echo; exit 1; fi
    echo
    make srpm

    read -p "Ready to build RPM on Fedora Copr ? (y/n) " -n 1
    if [[ ! $REPLY =~ ^[Yy]$ ]]; then echo; exit 1; fi
    echo
    copr-cli build tblock ~/rpmbuild/SRPMS/tblock-${PKGVERSION}-1.src.rpm
fi
