#!/usr/bin/env python

import re
import sys
import os.path

README = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'README.md')

if __name__ == '__main__':
    if len(sys.argv) == 1:
        sys.exit(f'usage: {sys.argv[0]} <output_file>')
    else:
        with open(README, 'rt') as f:
            readme = f.readlines()
        new_readme = '# TBLOCK\n\nAn anti-capitalist ad-blocker that uses the hosts file\n\n'
        for line in readme:
            if re.match(r'^## ', line):
                new_readme += line.upper()
            elif re.match(r'^!\[[A-z ]*]', line) or re.match(r'^\[!\[[A-z ]*]', line):
                pass
            else:
                new_readme += line
        new_readme = new_readme.replace('[@tblock@fosstodon.org](https://fosstodon.org/@tblock)',
                                        '@tblock@fosstodon.org')
        new_readme = new_readme.replace('[[PGP key](https://tblock.codeberg.page/uploads/keys/twann.asc)]', '')
        with open(sys.argv[1], 'wt') as f:
            f.write(new_readme)
