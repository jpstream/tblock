#!/usr/bin/env python

import os
import sys

# Declare variables to avoid text editor thinking there are errors
run_help = ''
run_converter_help = ''

# Get the variables of tblock.__init__ without importing the module
# Inspired from:
#       The setup.py script of: https://github.com/ytdl-org/youtube-dl/, which is released under Unlicense.
exec(compile(
    open(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'tblock', '__init__.py')).read(),
    os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'tblock', '__init__.py'), 'exec'
))

README = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'README.md')
AUTHORS = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'AUTHORS')
EXAMPLE_DIR = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'docs', 'examples')


def get_name(program: str) -> str:
    if program == 'tblock':
        return '# NAME\n\ntblock - an anti-capitalist ad-blocker that uses the hosts file\n\n'
    elif program == 'tblockc':
        return '# NAME\n\ntblockc - tblock\'s built-in filter converter\n\n'
    else:
        return ''


def get_synopsis(program: str) -> str:

    synopsis = '# SYNOPSIS\n\n'

    if program == 'tblock':
        usage = run_help.split('usage: ')[1].split('\n')[0]
        synopsis += f'**{usage.split(" ")[0]}** {usage.split(usage.split(" ")[0])[1]}'.replace('>', '\\>')

    elif program == 'tblockc':
        usage = run_converter_help.split('usage: ')[1].split('\n')[0]
        synopsis += f'**{usage.split(" ")[0]}** {usage.split(usage.split(" ")[0])[1]}'.replace('>', '\\>')

    return f'{synopsis}\n\n'


def get_description() -> str:
    with open(README, 'rt', encoding='utf-8') as f:
        desc = f.read().split('\n## About\n\n')[1].split('\n\n## Features')[0]
    return f'# DESCRIPTION\n\n{desc}\n\n'


def get_operations() -> str:

    output = '# OPERATIONS\n\n'

    for option_line in run_help.split('\n<operations>\n')[1].split('\n<options>\n')[0].split('\n'):
        if option_line[0:2] == '* ':
            output += f'## {option_line[2:].upper()}\n\n'
        elif option_line[0:3] == '  -':
            output += f'**{option_line[2:].split("  --")[0]}, -\\-{option_line.split("  --")[1].split(" ")[0]}**'
            for argument in option_line.split("  --")[1].split('['):
                if not option_line.split("  --")[1].split('[').index(argument) == 0:
                    output += f' _{argument.split("]")[0].split(" ")[0].upper()}_'
            desc = option_line.split("  --")[1].split("  ")[len(option_line.split("  --")[1].split("  ")) - 1].replace('--', '-\\-')
            output += f'\n: {desc}\n\n'

    return output


def get_options(program: str) -> str:

    output = '# OPTIONS\n\n'

    if program == 'tblock':
        for option_line in run_help.split('\n<options>\n')[1].split('\nFor more info')[0].split('\n'):
            if option_line[0:2] == '* ':
                output += f'## {option_line[2:].upper()}\n\n'
            elif option_line[0:3] == '  -':
                output += f'**{option_line[2:].split("  --")[0]}, -\\-{option_line.split("  --")[1].split(" ")[0]}**'
                for argument in option_line.split("  --")[1].split('['):
                    if not option_line.split("  --")[1].split('[').index(argument) == 0:
                        output += f' _{argument.split("]")[0].split(" ")[0].upper()}_'
                desc = option_line.split("  --")[1].split("  ")[len(option_line.split("  --")[1].split("  ")) - 1].replace('--', '-\\-')
                output += f'\n: {desc}\n\n'

    elif program == 'tblockc':
        for option_line in run_converter_help.split('\n<options>\n')[1].split('\nFor more info\n')[0].split('\n'):
            if option_line[0:2] == '* ':
                output += f'## {option_line[2:].upper()}\n\n'
            elif option_line[0:3] == '  -':
                output += f'**{option_line[2:].split("  --")[0]}, -\\-{option_line.split("  --")[1].split(" ")[0]}**'
                for argument in option_line.split("  --")[1].split('['):
                    if not option_line.split("  --")[1].split('[').index(argument) == 0:
                        output += f' _{argument.split("]")[0].split(" ")[0].upper()}_'
                desc = option_line.split("  --")[1].split("  ")[len(option_line.split("  --")[1].split("  ")) - 1].replace('--', '-\\-')
                output += f'\n: {desc}\n\n'

    return output


def get_examples(program: str) -> str:

    output = ''

    if program == 'tblock':
        with open(os.path.join(EXAMPLE_DIR, 'TBLOCK.md'), 'rt') as f:
            output = f.read()

    elif program == 'tblockc':
        with open(os.path.join(EXAMPLE_DIR, 'TBLOCKC.md'), 'rt') as f:
            output = f.read()

    return f'{output}\n'


def get_see_also(program: str) -> str:

    see_also = '# SEE ALSO\n\n'

    if program == 'tblock':
        see_also += '**tblockc**(1), **hosts**(5), **dnsmasq**(8)'

    elif program == 'tblockc':
        see_also += '**tblock**(1), **hosts**(5), **dnsmasq**(8)'

    return f'{see_also}\n\n'


def get_links() -> str:
    return '# LINKS\n\n- Website: _https://tblock.codeberg.page/_\n' \
           '- Wiki: _https://codeberg.org/tblock/tblock/wiki/Home_\n' \
           '- About the project: _https://tblock.codeberg.page/about_\n' \
           '- FAQ: _https://codeberg.org/tblock/tblock/wiki/FAQ_\n' \
           '- License: _https://www.gnu.org/licenses/gpl-3.0.html_\n\n'


def get_license() -> str:
    return '# LICENSE\n\nTBlock and its converter are released under **GPLv3**.\n\n'


def get_authors() -> str:
    authors = '# AUTHORS\n\n'
    with open(AUTHORS, 'rt', encoding='utf-8') as f:
        author_list = f.readlines()
    for a in author_list:
        authors += f'- **{a.split(" <")[0]}** <_{a.split(" <")[1].split(">")[0]}_>\n'
    return f'{authors}\n'


def _tblock() -> None:
    print(
        f'%TBLOCK(1)\n\n'
        f'{get_name("tblock")}'
        f'{get_synopsis("tblock")}'
        f'{get_description()}'
        f'{get_operations()}'
        f'{get_options("tblock")}'
        f'{get_examples("tblock")}'
        f'{get_links()}'
        f'{get_license()}'
        f'{get_see_also("tblock")}'
        f'{get_authors()}'
    )


def _tblockc() -> None:
    print(
        f'%TBLOCKC(1)\n\n'
        f'{get_name("tblockc")}'
        f'{get_synopsis("tblockc")}'
        f'{get_description()}'
        f'{get_options("tblockc")}'
        f'{get_examples("tblockc")}'
        f'{get_links()}'
        f'{get_license()}'
        f'{get_authors()}'
        f'{get_see_also("tblockc")}'
    )


def run_error_message() -> None:
    print(f'usage: {sys.argv[0]} [tblock|tblockc]')


if __name__ == '__main__':
    if len(sys.argv) == 1:
        run_error_message()
    elif len(sys.argv) == 2:
        if sys.argv[1] == 'tblock':
            _tblock()
        elif sys.argv[1] == 'tblockc':
            _tblockc()
        else:
            run_error_message()
    else:
        run_error_message()
