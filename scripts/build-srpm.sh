#!/usr/bin/env bash

# Change the directory where the script is executed from
cd "$(dirname "$0")/.." || exit

# Check if the Makefile exists, exit with error code 1 if it doesn't
if ! [[ -f "Makefile" ]]; then
  echo "ERROR: cannot find Makefile"
  exit 1
fi

CURRENT_DIR=$(pwd)

# Load package information
source scripts/package_info.sh

# Clean old workspace
rm -rf ~/rpmbuild

# Setup the build workspace
rpmdev-setuptree

# Retrieve the sources
wget https://codeberg.org/tblock/tblock/archive/$PKGVERSION.tar.gz -O ~/rpmbuild/SOURCES/$PKGVERSION.tar.gz
tar -xvf ~/rpmbuild/SOURCES/$PKGVERSION.tar.gz -C ~/rpmbuild/SOURCES/
mv ~/rpmbuild/SOURCES/tblock ~/rpmbuild/SOURCES/tblock-$PKGVERSION
rm -f ~/rpmbuild/SOURCES/$PKGVERSION.tar.gz
cd ~/rpmbuild/SOURCES
tar -czf ~/rpmbuild/SOURCES/$PKGVERSION.tar.gz tblock-$PKGVERSION
cd $CURRENT_DIR
rm -rf ~/rpmbuild/SOURCES/tblock-$PKGVERSION

# Build the source package
rpmbuild -bs tblock.spec
