# EXAMPLES

**tblockc -g** _/path/to/local/file.txt_
:  Detect the syntax of a local filter

**tblockc** _/path/to/local/file.txt_ **-s** adblockplus **-o** _output.txt_
:  Convert a local filter into "output.txt" using adblockplus syntax

**tblockc** _/path/to/local/file.txt_  **-s** hosts **-o** _output.txt_ **-0**
:  Convert a local filter into "output.txt" using hosts file syntax and 0.0.0.0 as default blocking IP

**tblockc** _/path/to/local/file.txt_ **-s** dnsmasq **-o** _output.txt_ **-i** tblock
:  Convert a local filter written in tblock syntax into "output.txt" using dnsmasq config syntax

**tblockc** _/path/to/local/file.txt_ **-s** adblockplus **-o** _output.txt_ **-c**
:  Convert a local filter and its comments into "output.txt" using adblockplus syntax
