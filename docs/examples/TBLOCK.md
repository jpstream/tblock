# EXAMPLES

**tblock -a** _example.org_ _example.com_
:  Allow two domains

**tblock -b** _example.org_
:  Block a domain

**tblock -r** _example.org_ 127.0.0.1
:  Redirect a domain to 127.0.0.1

**tblock -Sy** _easylist_
:  Subscribe to a filter and sync the remote filter repository

**tblock -C** _custom-filter_ _https://example.org/hosts.txt_
:  Subscribe to a custom filter

**tblock -M** ab _easylist_ _custom-filter_
:  Allow two filters to add allowing and blocking rules

**tblock -X** _custom-filter_ _custom-filter-renamed_
:  Rename a custom filter

**tblock -U**
:  Update all filters

**tblock -R** _easylist_
:  Remove a filter

**tblock -Lkw**
:  List filters you are subscribing to that are available on the remote repository
