# Filter syntax currently supported

| Name | About |
| --- | --- |
| adblockplus | AdBlock Plus syntax |
| dnsmasq | dnsmasq.conf syntax |
| hosts | Hosts file format |
| list | Simple domain blacklist |
| tblock | TBlock filter syntax |
| opera | Opera filter syntax |

If you would like another syntax to be added, [open an issue](https://codeberg.org/tblock/tblock/issues).
