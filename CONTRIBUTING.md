# Contributing to TBlock

Contributions are welcome from anybody. If you have a feature to request or a bug to report, you can [open an issue](https://codeberg.org/tblock/tblock/issues/) or [start a pull request](https://codeberg.org/tblock/tblock/pulls).

If you are interested to join the TBlock team and to work with us, please [send us an Email](mailto:twann@ctemplar.com?subject=I%20would%20like%20to%20join%20the%20TBlock%20team). It would be really nice to have a lot of people working on this project.

## Documentation

- [Pull request template](https://codeberg.org/tblock/tblock/src/branch/main/.gitea/pull_request_template.md)

## Repository

All commits to the `main` branch **must be signed**. Otherwise, Gitea will reject pushing them upstream.

## Code conventions

### Indent

Following the Python convetion, TBlock uses four spaces for each indent. For example:

```python
if condition:
    return 'yes'
else:
    return 'no'
```

### Inline conditions

TBlock uses inline conditions only when it is useful to use it. For example, if the value of a variable depends of a single condition, it is useful, otherwise not.

```python
var = 1 if condition else 2
```

```python
var = 1
if condition:
    var += 4
else:
    var = 2
```

### Conditions

Multiple conditions may be placed at the same line. This is only possible when it is easy to understand what the condition does.

For example, the following example SHOULD NOT BE USED, because it is almost not readable:

```python
if is_true() and 'k' not in answer() and answer() in sys.name and not is_false():
```

A good example would be (much easier to understand):

```python
if is_true() and 'k' not in answer():
    if answer() in sys.name and not is_false():
```

### Imports

Modules imported must be placed at the beginnig of the file, under the licensing information. You also need to specify with a comment if the module is a python built-in module, if it is a part of TBlock, or if it is an external module:

```python
# Standard libraries
import json
import os.path
import sqlite3
from xml.etree import ElementTree

# External libraries
import requests
import urllib3
from colorama import Fore, Style

# Local libraries
from .filters import Filter, get_all_from_rfr
from .config import Path, Var, Font
from .utils import download_file, check_root, lock_database, unlock_database
from .exceptions import InvalidLinkError, NetworkError, SIGTERM
```

### Comments

Comments	must be placed before the code piece to be commented:

```python
# These six lines checks the OS of the user
if sys.name == 'posix':
    system = 'linux'
elif sys.name == 'nt':
    system = 'windows'
else:
    system = 'unknown'

# This line prints the OS of the user
print(system)
```

### Functions

Every function is described, with its arguments and a brief overview of what is does/returns, like the example below:

```python
def example(var: str) -> str:
    """This is only an example
    
    Args:
        var (str): A random string
    
    Returns:
        str: A random output
    """
```

Function names must provide a brief overview of what the function does and words must be separated with an underscore, like that:

```python
def remove_all_files() -> None:
    """Remove all files on the computer
    """
```

## Classes

Each class must be followed with a line break. It also needs to provide a brief overview of what it does, and words must NOT be separated, like below:

```python
class Rule

    def __init__(self) -> None:
        """Rule object
        """
```

## Line breaks

Between to big conditions or loops, a line break is required. For example, in TBlock code:

```python
# The script is running on Termux
if os.path.isdir('/data/data/com.termux/files/usr/lib/'):
    PREFIX = '/data/data/com.termux/files/usr/lib/tblock/'
    HOSTS = '/system/etc/hosts'
    TMP_DIR = '/data/data/com.termux/files/usr/tmp/tblock/'

# The script is running on a POSIX platform
elif os.name == 'posix':
    PREFIX = '/var/lib/tblock/'
    HOSTS = '/etc/hosts'
    TMP_DIR = '/tmp/tblock/'

# The script is running on Windows
elif os.name == 'nt':
    PREFIX = os.path.join(os.path.expandvars('%ALLUSERSPROFILE%'), 'TBlock')
    HOSTS = os.path.join(os.path.expandvars('%WINDIR%'), 'System32', 'drivers', 'etc', 'hosts')
    TMP_DIR = os.path.join(os.path.expandvars('%TMP%'), 'tblock')

# If the script is running on an unsupported platform, raise an error
else:
    raise OSError('TBlock is currently not supported on your operating system')
```

If the condition is not big, there is no need to add a line break between each condition, but only between the condition statement and the code:

```python
class Font:
    BOLD = '\033[1m'
    DEFAULT = '\033[0m'
    UNDERLINE = '\033[4m'

    # The check mark appears weird on Windows, so replace it by 'v'
    if os.name == 'nt':
        CHECK_MARK = 'v'
    else:
        CHECK_MARK = '✓'
```

Between two classes or two functions, you need to add two line breaks IF THE FILE CONTAINS CLASSES:

```python
class SomeClass:
    
    def __init__(self) -> None:
        pass


class AnotherClass:
    
    def __init__(self) -> None:
        pass


def some_function() -> None:
    pass
```

If the file does not contain any class, only one line break is required:

```python
def some_function() -> None:
    pass

def another_function() -> None:
    pass
```


If two functions are inside a class, only one line break is required:

```python
class SomeClass:
    
    def __init__(self) -> None:
        pass
    
    def some_function(self) -> None:
        pass
```

At the end of the file, no line break is needed:

```python
def something() -> None:
    pass  # This is the end of the file
```
