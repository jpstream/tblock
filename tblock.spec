Name:           tblock
Version:        1.3.0
Release:        1%{?dist}
Summary:        An anti-capitalist ad-blocker that uses the hosts file

License:        GPLv3
URL:            https://tblock.codeberg.page
Source0:        https://codeberg.org/tblock/tblock/archive/%{version}.tar.gz

BuildRequires:  make
BuildRequires:  pandoc
BuildRequires:  gzip
BuildRequires:  python
BuildRequires:  python-setuptools
Requires:       python
Requires:       python-colorama
Requires:       python-requests
Requires:       python-urllib3

%description
TBlock is a system-wide, platform independent ad-blocker written in Python and releas
ed under GPLv3.

%global debug_package %{nil}


%prep
%autosetup


%build
make 

%install
rm -rf %{buildroot}
%make_install


%files
%license LICENSE
%doc README.txt
/usr/bin/tblock
/usr/bin/tblockc
/usr/share/man/man1/tblock.1.gz
/usr/share/man/man1/tblockc.1.gz
/usr/lib/python3.*/site-packages/tblock/
/usr/lib/python3.*/site-packages/tblock-*-py3.*.egg-info/


%changelog
* Mon Jun 7 08:42:30 CEST 2021 Twann <twann@ctemplar.com>
- Published version 1.0.1 to COPR

* Fri Jun 4 18:23:30 CEST 2021 Twann <twann@ctemplar.com>
- Published version 1.0.0 to COPR

* Sun May 30 14:52:30 CEST 2021 Twann <twann@ctemplar.com>
- Published version 0.0.6 to COPR

