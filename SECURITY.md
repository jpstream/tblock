# Security

TBlock developers take security very seriously. 
If you found a security issue, please:

- **DO NOT OPEN** a public issue
- Instead, contact us via Email to `tblock@e.email` [PGP key](https://tblock.codeberg.page/uploads/keys/tblock.asc)

