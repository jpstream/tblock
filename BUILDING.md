# Build instructions

To build TBlock manually, you need to install the following dependencies:
`python` `python-setuptools` `pandoc` `make` `gzip`

## Build TBlock

To build TBlock, simply enter follow the steps below:

1. Clone the Git repository

```sh
git clone --depth=1 https://codeberg.org/tblock/tblock.git tblock
```

2. Enter the directory

```sh
cd tblock
```

3. Build TBlock and its files

```sh
make
```

## Install TBlock after building

To install TBlock after building it manually, simply run:

```sh
sudo make install
```

This will install TBlock and its files in the root filesystem (`/`) and its subdirectories like `/usr/bin/`. To change the default destination directory, simply change the value of the `DESTDIR` variable by executing the following:

```sh
sudo make install DESTDIR=path/to/directory
```

