# Versioning

Each TBlock version contains three numbers separated by dots:
`MAJOR.MINOR.PATCH`

## Examples

For example, in `1.2.3`:

- `1` would be the major version
- `2` would be the minor version
- `3` would the patch version

## Explanations

- Every time an issue is fixed, the new release will only change the patch number.
- Every time a new feature is introduced, the new release will change the minor number.
- When the code is re-written or when there are really big changes to the python module, the new release will change the major number.
