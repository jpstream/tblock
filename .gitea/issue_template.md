### Read this before opening an issue

Before opening an issue, please ensure that:

- [ ] You are running **the latest version** of TBlock
- [ ] The issue IS NOT on the [known issues page of the wiki](https://codeberg.org/tblock/tblock/wiki/Known-issues).
- [ ] The issue you found has not already been reported on [the issue tracker](https://codeberg.org/tblock/tblock/issues).

Also, do not open an issue like that:

```
It does not work !
```

If you want to help the project being improved and your problem to be fixed, you need to: 

- [ ] Describe what happened
- [ ] Specify which platform you are running (for ex. the name of your Linux distribution, Windows, etc.)
- [ ] Maybe add a screenshot or the error text
- [ ] Be polite

**DO NOT FORGET TO DELETE THIS TEXT BEFORE OPENING YOUR ISSUE**
