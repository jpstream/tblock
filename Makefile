#!/usr/env/bin make

all: tblock.1.gz tblockc.1.gz README.txt build

build:
	$(PYTHON) setup.py clean --all
	$(PYTHON) setup.py build


.PHONY: all clean pypi install codetest test srpm uninstall

PYTHON ?= /usr/bin/env python3
DESTDIR ?= /

install: all
	install -d $(DESTDIR)/usr
	install -d $(DESTDIR)/usr/bin
	install -d $(DESTDIR)/usr/share/man/man1
	install -m 644 tblock.1.gz $(DESTDIR)/usr/share/man/man1
	install -m 644 tblockc.1.gz $(DESTDIR)/usr/share/man/man1
	install -d $(DESTDIR)/usr/share/doc/tblock
	install -m 644 README.txt $(DESTDIR)/usr/share/doc/tblock
	install -d $(DESTDIR)/usr/share/licenses/tblock
	install -m 644 LICENSE $(DESTDIR)/usr/share/licenses/tblock
	$(PYTHON) setup.py install --root="$(DESTDIR)" --optimize=1 --skip-build

uninstall:
	rm -f $(DESTDIR)/usr/share/man/man1/tblock.1.gz
	rm -f $(DESTDIR)/usr/share/man/man1/tblockc.1.gz
	rm -rf $(DESTDIR)/usr/share/doc/tblock
	rm -rf $(DESTDIR)/usr/share/licenses/tblock
	$(PYTHON) -m pip uninstall tblock --yes

clean:
	rm -rf dist/ build/ *.egg-info/ **/__pycache__/ tblock.1.gz tblockc.1.gz \
		README.txt *_man.md new_version_changelog.tmp pkg/ \
		test/root/[abdceghiklmnopqrst]*.[0127bcefilnoqstx]* \
		test/root/hosts* test/root/repo test/root/verification \
		test/root/.db-lock tblock.bin tblockc.bin *.backup \
		run-tblock*.py scripts/nsis/*.spec scripts/nsis/tblock*.exe \
		scripts/nsis/build/ scripts/nsis/dist/ test/root/.needs-update

srpm: all
	bash scripts/build-srpm.sh

README.txt:
	$(PYTHON) scripts/format-text.py README.txt
	pandoc -s -f markdown -t plain README.txt -o README.txt

tblock.1.gz:
	$(PYTHON) scripts/gen-manpages.py tblock > tblock_man.md
	pandoc -s -f markdown -t man tblock_man.md -o tblock.1
	gzip tblock.1 -f
	rm -f tblock_man.md

tblockc.1.gz:
	$(PYTHON) scripts/gen-manpages.py tblockc > tblockc_man.md
	pandoc -s -f markdown -t man tblockc_man.md -o tblockc.1
	gzip tblockc.1 -f
	rm -f tblockc_man.md

pypi:
	rm -rf pkg/pypi-dist/
	$(PYTHON) setup.py -q sdist --dist-dir pkg/pypi-dist/ bdist_wheel --dist-dir pkg/pypi-dist/

venv:
	mkdir "venv"
	$(PYTHON) -m virtualenv -q "venv" --clear
	venv/bin/python3 -m pip -q install --upgrade pip setuptools wheel build twine
	venv/bin/python3 -m pip -q install -r requirements.txt
	venv/bin/python3 setup.py -q install --prefix venv

codetest:
	$(PYTHON) -m flake8 .

test: codetest
	rm -f test/root/.db-lock
	$(PYTHON) -m nose test -v

