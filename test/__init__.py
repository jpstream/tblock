#!/usr/bin/env python

import os
import tblock.config
import tblock.exceptions
from tblock import repo
import unittest
from nose.tools import raises

tblock.config.Path.HOSTS = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'root', 'hosts')
tblock.config.Path.DATABASE = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'root', 'storage.sqlite')
tblock.config.Path.DB_LOCK = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'root', '.db-lock')
tblock.config.Path.REPO_VERSION = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'root', 'repo')
tblock.config.Path.NEEDS_UPDATE = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'root', '.needs-update')
tblock.config.Path.HOSTS_VERIFICATION = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'root', 'verification')
tblock.config.setup_database(tblock.config.Path.DATABASE)

real_repo_mirrors = tblock.config.Var.REPO_MIRRORS

tblock.config.Var.REPO_MIRRORS = ['http://localhost:8238/SfcjUoLAOncidnIDSN/263h/huwfUFB%20Jdsfh/file.txt']


class TestRepo(unittest.TestCase):

    @raises(tblock.exceptions.NetworkError)
    def test_sync_remote_repo_fail(self):
        self.assertEqual(
            repo.sync_remote_repo(force=True),
            None
        )

    def test_sync_remote_repo_success(self):
        tblock.config.Var.REPO_MIRRORS = real_repo_mirrors
        try:
            os.remove(tblock.config.Path.DB_LOCK)
        except FileNotFoundError:
            pass
        self.assertEqual(
            repo.sync_remote_repo(force=True),
            None
        )


if __name__ == '__main__':
    try:
        os.remove(tblock.config.Path.DB_LOCK)
    except FileNotFoundError:
        pass
    unittest.main()
