[Adblock Plus 3.1]
! Title: Twann's List
! Version: 3.0.1
! Description: A powerful filter, combining security, privacy protection, ad blocking, and social icons and annoyances blocking.
! Last modified: 19 Apr 2020 23:37 UTC
! Expires: 4 days
! Homepage: https://codeberg.org/twann/filters
! License: https://codeberg.org/twann/filters/src/branch/master/LICENSE
! Email: twann@ctemplar.com
! Issues: https://codeberg.org/twann/filters/issues
!
! {WHITELIST}
!
! [CryptPad]
cryptpad.fr#@#.cp-privacy:not(body):not(html)
!
! [TBlock]
@@||tblock.codeberg.page^
!
! {DOMAINS}
!
! [000WebHostApp]
||cdn.000webhost.com^$third-party,cookie,script
||hostinger.com^$third-party
.000webhost.com/js/vendor.js*$script
!
! [2o7.net]
||grunerandjahr.112.2o7.net^
!
! [AddAppTr]
||addapptr.com^$third-party
!
! [AdGuard]
||stat.adguard.com^
!
! [Adobe]
||adobedmt.com^
||adobedc.net^
||analytics.adobe.com^$third-party
||adobedtm.com^
||adobe-analytics.*
||adobe-analytics.net^
||adobe-analytics.com^
||demdex.net^
||dpm.demdex.net^
!
! [AdSafe Protected]
||cdn.adsafeprotected.com^$third-party
!
! [AdsKeeper]
||adskeeper.*$third-party
!
! [Ajoutez Votre Site]
||ajoutezvotresite.com^$third-party
!
! [Amazon]
||amazon-adsystem.com^
||z-na.amazon-adsystem.com^
||c.amazon-adsystem.com^
||mobileanalytics.us-east-1.amazonaws.com^
||associates.amazon.com^$third-party
||certify-js.alexametrics.com^
||alexametrics.com^
!
! [Amp Project]
||ampproject.org^$third-party
!
! [analysis.fi]
||measure.analysis.fi^
||ecdn.analysis.fi^
!
! [App Adjust]
||app.adjust.com^
!
! [App Measurement]
||app-measurement.com^$third-party
!
! [Apple]
||appstoreconnect.apple.com^
||analytics.itunes.apple.com^
||podcastsconnect.apple.com^
||banners.itunes.apple.com^
||metrics.apple.com^
||securemetrics.apple.com^
||appleid.cdn-apple.com^$third-party
!
! [AppNexus]
||secure.adnxs.com^
||ib.adnxs.com^
!
! [Avast]
||t.av.st^
!
! [Azure CloudApp]
||cloudapp.azure.com^
!
! [Betrad]
||betrad.com^$third-party
!
! [Bizible]
||cdn.bizible.com^$third-party
!
! [Browser Update]
||browser-update.org^
!
! [Bugly]
||bugly.qq.com^
!
! [BugsNag]
||bugsnag.com^$third-party
!
! [BuySell Ads]
||servedby-buysellads.com^
||buysellads.com^$third-party
!
! [capitalism.com]
||3c3om01yrod0fs2t838h82el-wpengine.netdna-ssl.com^
!
! [Consensu]
||consensu.org^$third-party
||consentmanager.mgr.consensu.org^
||quantcast.mgr.consensu.org^
!
! [CookieBot]
||cookiebot.com^$third-party
||consent.cookiebot.com^
!
! [Cookie Consent]
||cookieconsent.com^$third-party
!
! [CrazyEgg]
||crazyegg.com^$third-party
!
! [Data Browser Agent]
||datadoghq-browser-agent.com^
!
! [DeepL]
||deepl.com^$cookie=uid
||s.deepl.com/web/statistics
!
! [Disqus]
||disqus.com^$third-party
||androidauthority.disqus.com^
/disqus-count-*.js
!
! [DropBox]
||dropbox.com/log
||dropbox.com/alternate_*
!
! [DropWizard]
||metrics.dropwizard.io^
!
! [DuckDuckGo]
||improving.duckduckgo.com^
!
! [Facebook]
||ads.facebook.com^
||ads.facebook.net^
||analytics.facebook.com^
||analytics.facebook.net^
||appevents.facebook.com^
||audiencenetwork.facebook.com^
||cdn.fbsbx.com^$third-party
||places.facebook.com^
||share.facebook.com^
||connect.facebook.com^
||connect.facebook.net^
||lookaside.facebook.com^
||login.facebook.com^$third-party
&fbclid=
&fb_action_ids=
&fb_comment_id=
&fb_action_types=
&fb_ref=
&fb_source=
||facebook.com^$third-party,cookie=c_user
||instagram.com/logging*
/fbevents.js
!
! [First Impression]
||ecdn.firstimpression.io^
||publishers.firstimpression.io^
||firstimpression.io^$third-party
!
! [Ghostery]
||analytics.ghostery.com^
!
! [GitLab]
||assets.gitlab-static.net/assets/snowplow/*
!
! [Gleam]
||gleam.io^$third-party
||js.gleam.io^
!
! [Go-Mpulse]
||s.go-mpulse.net^
||c.go-mpulse.net^
||ip46.go-mpulse.net.edgekey.net^
||wildcard46.go-mpulse.net.edgekey.net^
||go-mpulse.net^$third-party
!
! [GoDaddy]
||a-fds.youborafds01.com^
!
! [Google]
||2mdn.net^
||dmtry.com^
||doubleclick.com^
||doubleclick.net^
||mng-ads.com^
||adservice.google.com^
||adservice.io^
||googleadservices.com^
||feedads.googleadservices.com^
||partner.googleadservices.com^
||www.googleadservices.com^
||9098b63249ee4522c5d8677059fe22e4.safeframe.googlesyndication.com^
||pagead-googlehosted.l.google.com^
||firebase.com^
||firestore.googleapis.com^
||firebaseremoteconfig.googleapis.com^
||firebaseinstallations.googleapis.com^
||firebaselogging.googleapis.com^
||firebase-settings.crashlytics.com^
||try.crashlytics.com^
||settings.crashlytics.com^
||crashlytics.l.google.com^
||crashlytics.com^
||b.scorecardresearch.com^
||tpc.googlesyndication.com^
||google-analytics.com^
||www.google-analytics.com^
||analytics.withgoogle.com^
||googleanalytics.com^
||ssl.google-analytics.com^
||www-google-analytics.l.google.com^
||ssl-google-analytics.l.google.com^
||google-analytics.l.google.com^
||g.doubleclick.net^
||securepubads.g.doubleclick.net^
||displayads-formats.googleusercontent.com^
||pagead46.l.doubleclick.net^
||partnerad.l.doubleclick.net^
||buttons.googlesyndication.com^
||pagead1.googlesyndication.com^
||pagead2.googlesyndication.com^
||pagead3.googlesyndication.com^
||pagead.googlesyndication.com^
||googleads2.g.doubleclick.net^
||googleads6.g.doubleclick.net^
||googleads7.g.doubleclick.net^
||googleads4.g.doubleclick.net^
||2542116.fls.doubleclick.net^
||9309168.fls.doubleclick.net^
||googleads.g.doubleclick.net^
||static.doubleclick.net^
||cm.g.doubleclick.net^
||pubads.g.doubleclick.net^
||www.googletagservices.com^
||www.googleoptimize.com^
||cse.google.com^
||video-stats.video.google.com^
||clientmetrics-pa.googleapis.com^
||mail-ads.google.com^
||mimageads.googleadservices.com^
||mimageads1.googleadservices.com^
||mimageads2.googleadservices.com^
||mimageads3.googleadservices.com^
||mimageads4.googleadservices.com^
||mimageads5.googleadservices.com^
||mimageads6.googleadservices.com^
||mimageads7.googleadservices.com^
||mimageads8.googleadservices.com^
||mimageads9.googleadservices.com^
||adsense.google.com^
||googleads.*^
||b.scorecardresearch.com^
||scorecardresearch.com^$third-party
||googlesyndication.com^$third-party
||googleoptimize.com^$third-party
||www.googleoptimize.com^
/google_ads_prefetch.js$
/adsense/*
/doubleclick
/pagead2.
/adsbygoogle.
/pagead.
/securepubads.
/pubads.
/firebasejs*
/googlads.
&utm_source=
&utm_medium=
&utm_term=
&utm_content=
&utm_campaign=
&utm_referrer=
&yclid=
&gclid=
&cr=$domain=~.google.com^
$cookie=/__utm[a-z]/
!
! [Herow]
||herow.io^$third-party
!
! [HotJar]
||hotjar.com^$third-party
||static.hotjar.com^
!
! [Hs-Scripts]
||js.hs-scripts.com^$third-party
!
! [INFOnline]
||infonline.de^$third-party
!
! [InLoco]
||inloco.com.br^$third-party
!
! [InstaBug]
||instabug.com^$third-party
!
! [Instant Page]
||instant.page^$document
!
! [Iron Source]
||ironsrc.com^$third-party
||ironsource.com^$third-party
!
! [Jumbo Privacy]
||analytics.jumboprivacy.com^
!
! [Kidoz]
||sdk.kidoz.net^$third-party
||kidoz.net^$third-party
!
! [Klick]
||trk.klclick.com^
||klclick.com^
!
! [MalwareBytes]
||telemetry.malwarebytes.com^
!
! [Mapbox]
||a.tiles.mapbox.com^
||api.tiles.mapbox.com^
! Optimizely
||optimizely.com^$third-party
||ogx.optimizely.com^
!
! [MathTag]
||pixel.mathtag.com^
||pixel.mathtag.com.edgekey.net^
||e6791.b.akamaiedge.net^
!
! [Matomo / Piwik]
||piwik.*.*^$third-party
||piwik.biblissima.fr^
/piwik.
/matomo.
!
! [Maxymiser]
||service.maxymiser.net^
!
! [Meriam-Webster]
||stats.merriam-webster.com^
!
! [Microsoft]
||appcenter.ms^
||in.appcenter.ms^
||appcenter.microsoft.com^
||appcenteranalytics.microsoft.com^
||analytics.microsoft.com^
||vortex.data.microsoft.com^
||web.vortex.data.microsoft.com^
||pipe.aria.microsoft.com^
||browser.pipe.aria.microsoft.com^
||mobile.events.data.microsoft.com^
||browser.events.data.microsoft.com^
||self.events.data.microsoft.com^
||target.microsoft.com^
||crashes.microsoft.com^
||analytics.microsoft.com^
||collector.githubapp.com^
||bingads.microsoft.com^
||m.bingads.microsoft.com^
||fp.advertising.microsoft.com^
||advertise.bingads.microsoft.com^
||smetric.ads.microsoft.com^
||sqm.telemetry.microsoft.com^
||telemetry.microsoft.com^
||telecommand.telemetry.microsoft.com^
||reports.wes.df.telemetry.microsoft.com^
||df.telemetry.microsoft.com^
||wes.df.telemetry.microsoft.com^
||dc.ads.linkedin.com^
||snap.licdn.com^
||px.ads.linkedin.com^
*ref=$domain=.microsoft.com^
||microsoft.com/_log?
$cookie=MicrosoftApplicationsTelemetryFirstLaunchTime
$cookie=MicrosoftApplicationsTelemetryDeviceId
!
! [Moat]
||apx.moatads.com^
||geo.moatads.com^
||js.moatads.com^
||mb.moatads.com^
||moat.com^$third-party
||pixel.moatads.com^
||px.moatads.com^
||sejs.moatads.com^
||yt.moatads.com^
||yts.moatads.com^
||z.moatads.com^
!
! [MotherShip Tools]
||data.mothership.tools^
!
! [Mozilla]
||telemetry.mozilla.org^
||sql.telemetry.mozilla.org^
||gecko.mozilla.org^
!
! [Nativo]
||jadserve.postrelease.com^
!
! [NbCuni]
||mps.nbcuni.com^
||nbcuni.com^$third-party
!
! [New Relic]
||js-agent.newrelic.com^
||mobile-collector.newrelic.com^
||newrelic.com^$third-party
||nr-data.net^
||insights-collector.newrelic.com^
!
! [ntv.io]
||ntv.io^$third-party
!
! [Omniture]
||my.omniture.com^$third-party
||nbcume.sc.omtrdc.net^
||sc.omtrdc.net^
||omtrdc.net^
!
! [OneTrust]
||geolocation.onetrust.com^$third-party
!
! [Opmnstr]
||a.opmnstr.com^
||opmnstr.com^$third-party
!
! [Pendo]
||cdn.pendo.io^
!
! [PerfectAudience.com]
||tag.perfectaudience.com^
!
! [Pingdom.net]
||rum-static.pingdom.net^
!
! [Polyfill]
||cdn.polyfill.io^$third-party
||polyfill.io^$third-party
!
! [Pubdirecte]
||pubdirecte.com^
!
! [PubMatic]
||ads.pubmatic.com^
||aktrack.pubmatic.com^
||gads.pubmatic.com^
||image2.pubmatic.com^
||simage2.pubmatic.com^
!
! [Quantcast]
||quantcast.com^$third-party
||quantcast.net^
!
! [QuantServe]
||secure.qwantserve.com^
||pixel.quantserve.com^$third-party
||quantserve.com^$third-party
!
! [Queryly]
||queryly.com^$third-party
!
! [Quizlet]
||eventlogger-k8s.quizlet.com^
!
! [Sentry]
||sentry.io^$third-party
||reload.getsentry.net^
!
! [SnigelWeb]
||snigelweb.com^$third-party
||cdn.snigelweb.com^$third-party
!
! [RavenJS]
||cdn.ravenjs.com^$third-party
!
! [RjFun]
||adlic.rjfun.com^
!
! [SearchIQ]
||pub.searchiq.co^
!
! [SirData]
||choices.consentframework.com^
!
! [Skimlinks]
||s.skimresources.com^
!
! [Smart]
||adsrvr.org^
||akamai.smartadserver.com^
||cdn1.smartadserver.com^
||diff2.smartadserver.com^
||diff3.smartadserver.com^
||diff.smartadserver.com^
||eqx.smartadserver.com^
||gallery.smartadserver.com^
||im2.smartadserver.com^
||insight.adsrvr.org^
||itx5-publicidad.smartadserver.com^
||itx5.smartadserver.com^
||js.adsrvr.org^
||match.adsrvr.org^
||preview.smartadserver.com^
||rtb-csync.smartadserver.com^
||saspreview.com^
||smartadserver.com^$third-party
||smartadserver.ru^
||tmk.smartadserver.com^
||usw-lax.adsrvr.org^
!
! [SmartLook]
||lp.smartlook.com^
||app.smartlook.com^
!
! [Stack Sonar]
||stack-sonar.com^
!
! [Statcounter]
||statcounter.com^
!
! [StickyAdsTV]
||ads.stickyadstv.com^
!
! [Taboola]
||cdn.taboola.com^
!
! [Tinder]
||tinder-analytics.com^
||analytics.tinder.com^
||ads.tinder.com^
||tinderads.com^
!
! [Tpd Ads]
||tpdads.com^
||cdn.tpdads.com^
!
! [Traffic Junky]
||trafficjunky.*^
||trafficjunky.com^
||trafficjunky.net^
||hw-cdn2.ang-content.com^
||a.adtng.com^
$cookie=trafficJunkyPopsBackUrl
!
! [Traffic Manager]
||trafficmanager.net^
!
! [Twitter]
||analytics.twitter.com^
||dc.glbtls.t.co^
||ads-twitter.com^
||syndication.twitter.com^
||cdn.syndication.twimg.com^
||platform.twitter.com^
!
! [UnityCms]
||feed-prod.unitycms.io^$third-party
!
! [Unkwnown Crate]
||unknowncrate.com^$third-party
!
! [Waust]
||waust.at^$third-party
!
! [Wcitianka]
||7487.wcitianka.com^
!
! [WordPress]
||stats.wp.com^
!
! [Yadro]
||yadro.ru^$third-party
!
! [Yahoo]
||web.analytics.yahoo.com^
||syndication.twitter.com^
||cdn.syndication.twimg.com^
||platform.twitter.com^
!
! [Zeddit]
||hello.zeddit.com^$third-party
!
! [Zeotap]
||zeotap.com^$third-party
!
! {MALWARE DOMAINS}
||190-2-131-201.hosted-by-worldstream.net^
||d2ip7iv1l4ergv.cloudfront.net^
||d2tr3e2bwzpvfj.cloudfront.net^
||822f797ef9d68ad3.com^
||544bd1e82a9c8c39.com^
||e21b1642db801b2a.com^
||ded733b374cc1c3f.com^
||ae21a0ad95016488.com^
||82448dceaaa13034.com^
!
! {ADBLOCKPLUS SPECIFIC RULES}
/gdpr/*
/eventlogger*
||astatic.ccmbg.com/fc/js/hammer*
||my.freenom.com/includes/jscript/loggedon.php*
||cdn.cnn.com/ads/*
||c.evidon.com/pub/$image,third-party
||erdentec.com/wp-content/litespeed/*
/adsrvr.
/GetUserFromCookies
/sw-stats/eligible
/logo/footer-powered-by-*
/ads.
/jadserve.
/v2/tracker/*
/pub/ads/*
/tracker.$third-party
/trackers.$third-party
/analytics.$third-party
/servads.
/servead.
/optimizelyjs/
/counter.*
/zone_id
/zoneid
/AdsSettingsPage.
/metrics.
/metrics-*
/advertisement-*$important
/images/facebook.png$domain=win10.software
/images/twitter.png$domain=win10.software
/images/google_plus.png$domain=win10.software
/nbcu_gdpr_*
-ads.*
*-ads.
/api/stats/*
/metrics/mobile/*
/getCookie
*/metrics/*
/sw-stats/*
/collectConsent?
/copyConsent?
/ads-blocking-detector.*$script
*footer_promos_*
*devsite_analytics_*
||frog.wix.com/performance?*
||dna8twue3dlxq.cloudfront.net/js/profitwell.js$document
browser_performance_info
/client_metrics
/log_js_sw_data
/dist/ads.
/dist/log_
_appmeasurement*
/fuckadblock.*
&track=
&_openstat=
&action_object_map=
&action_type_map=
&action_ref_map=
&ref=
&trackingserver=
&refer=
&referrer=
&zoneid=
&zone_id=
&site_id=
&timezone=
&cbrand=
&cbr=
&cmodel=
&cos=
&cosver=
&cplatform=
&guccounter=
&guce_referrer=
&guce_referrer_sig=
$cookie=mParticleId
$cookie=optimizelyEndUserId
$cookie=uber_sites_geolocalization
$cookie=PersonalizationCookie
##.jBlockAmazon
##.adsbygoogle
##div#antiAdBlock.modal.fade
www.javascript.com###site-promo
time.com##div.magazine.desktop
time.com##section.homepage-module.newsletter
time.com##li.newsletter
time.com##a.newsletter-signup
time.com##a.subscribe-link.subx_track_952
sitepoint.com##div.roller-door-placeholder
sitepoint.com##[type="book"]
sitepoint.com##div[type="books_new"]
dailymotion.com##div.TCF2Banner__container___1D3J6
www.usmagazine.com##a.item.trio-promo__item
usanetwork.com##div.usa-home-popup-overlay.newsletter-popup-overlay.omniture-tracking-processed
##div.fanclub-info-pop.active
www.ps3cfw.com##div[role="alert"]
###banner_ad
rootmygalaxy.net###text-130
erdentec.com##div.sweet-overlay
erdentec.com##div.sweet-alert.showSweetAlert.visible
erdentec.com##div.san_howtowhitelist
wmtips.com##div.ma.img100
ciscossh.com###g207
dailymotion.com###root > div.Root__app___OOVS6.desktop > div.TCF2Banner__container___1D3J6
swisscows.com##div.banners-wrap
||swisscows.com/api/b4r/image/5d6c0b08665747eea0fc642451af69f8
geeksforgeeks.org##img.ad_course_banner
usanetwork.com##div.usa-home-popup-overlay.newsletter-popup-overlay.omniture-tracking-processed:nth-child(9)
startpage.com##div.layout:nth-child(3) > div.layout-web.layout-web--default:nth-child(2) > div.layout-web__body:nth-child(2) > div.layout-web__sidebar.layout-web__sidebar--web:last-child > div.widget-install-sp.widget-install-sp--default:nth-child(2) > div.widget-install-sp-container
usanetwork.com##div.usa-home-popup-overlay.newsletter-popup-overlay.omniture-tracking-processed:nth-child(10) > div.usa-newsletter-popup:last-child
usanetwork.com##div.usa-home-popup-overlay.newsletter-popup-overlay.omniture-tracking-processed:nth-child(11) > div.usa-newsletter-popup:last-child
hellomagazine.com###special_banner > a > picture > img:last-child
cloudways.com###post_sideBnr_boxWrap > section.pull-wide.cw-stky-campaign.cw-stky-campaign-1777.cw-stky-campaign-1488:first-child > a
cloudways.com###inrblog_btm_scrbBar
win10.software###content > div.inner_product_card:first-child > aside.R_sidebar:nth-child(3) > div.banner:last-child
7-zip.en.uptodown.com##footer:nth-child(12) > div.content.social:nth-child(2) > ul
softpedia.com###navicos > li.fa.fa-heart.nosel:first-child
softpedia.com###wrapper > div.main:last-child > div.statusbar.posrel:first-child > div.container_48 > div.grid_21.breadcrumb.flashsale.ta_right:nth-child(2)
pcmag.com###app > footer.my-12.bg-white:last-child > div.bg-black.text-white:last-child > div.container > div.mt-6.border-b.border-gray-darker.text-sm.pb-8.leading-tight.text-gray-base.text-center.relative:nth-child(2) > div.flex.flex-col.md\:flex-row.flex-wrap.mt-6:last-child > ul.w-full.justify-center.flex-shrink.flex.flex-wrap.mt-4.xl\:-mt-12.xl\:pt-3.xl\:w-auto.xl\:absolute.xl\:right-0:last-child > li.w-full:first-child > button.showConsentTool.my-4.block.mx-auto.xl\:float-right.xl\:mx-0.xl\:mb-2
pcmag.com###app > footer.my-12.bg-white:last-child > div.flex.flex-col.md\:flex-row.text-white.container:first-child
freewarefiles.com###wrapper > article:nth-child(6) > div.container > div.row:last-child > div.col-sm-3.left-nav-remove-list.clearfix.pulli-right.produc-right-sec:last-child > div.fb-page:nth-child(6) > blockquote.fb-xfbml-parse-ignore > a
freewarefiles.com###wrapper > article:nth-child(6) > div.container > div.row:last-child > div.col-sm-3.left-nav-remove-list.clearfix.pulli-right.produc-right-sec:last-child > div.gap:nth-child(7)
$$amp-ad
ciscossh.com###g207
people.com##main.home:nth-child(2) > section.component.category-page-tout-default.categoryPageToutDefault.category-page-tout--default:nth-child(12)
swisscows.com##div.web-results > div.banners-wrap
swisscows.com##div.web-results > article.item.item--web.sales
##script[tag-content="advertising|disabled"]
tecmint.com###ads
whois.com###page-wrapper > div.whois_page_wrapper > div.whois_rhs_column
inducesmile.com###custom_html-3 > div.textwidget.custom-html-widget > a > img
tonytony1312.wixsite.com###comp-k91qhncdinlineContent
tonytony1312.wixsite.com###tmdm5balatabgcoloroverlay
tonytony1312.wixsite.com###tmdm5balatabgcolor
github.com##div.js-cookie-consent-banner.hx_cookie-banner--show
quizlet.com##div.UIModal.UIModal-container.is-none.is-open.does-takeoverAtMedium.UIModal--small.StudyBreakAdzModal:last-child > div.UIModal-box > div.UIModalBody:last-child > div.UIDiv.StudyBreakAdzModal-wrapper
quizlet.com##div.UIModal.UIModal-container.is-none.is-open.does-takeoverAtMedium.UIModal--small.StudyBreakAdzModal:last-child > div.UIModal-box > div.UIModalHeader:first-child
quizlet.com##div.UIModal.UIModal-container.is-none.is-open.does-takeoverAtMedium.UIModal--small.StudyBreakAdzModal:last-child
quizlet.com##div.UIModal-backdrop.is-included.is-visible:nth-child(5)
oracle.com##div.ochat_slideout:nth-child(9) > ul.ochat_slidew2.active
oracle.com##div.f11w1:nth-child(2) > section.cb27.cb27v6.cb27sticky.rw-yellow-40bg.cb27show.cb27stuck:nth-child(12)
cloudways.com###post_sideBnr_boxWrap > section.pull-wide.cw-stky-campaign.cw-stky-campaign-2882.cw-stky-campaign-1477:first-child > a
cloudways.com##div.bfcmCouponDealsPopUpSec:nth-child(33)
averagelinuxuser.com##div.site-content:nth-child(3) > div.container:first-child > div.banner:first-child > figure.banner-figure > a > img
||gstatic.com/youtube/img/promos/growth/dmod_si_horizontal_ver1_240x400.png
protegez-vous.ca##.cc-window.cc-floating.cc-type-info.cc-theme-edgeless.cc-bottom.cc-left.cc-color-override-1338869719
youtube.com##.style-scope.yt-upsell-dialog-renderer
youtube.com###dialog
youtube.com##.opened
comparitech.com##.oui-modal
comparitech.com##.underlay
comparitech.com###ct-popup
comparitech.com##.ct_popup_modal.ct-nordvpn-bf-nov-2020
computer.howstuffworks.com##.banner_banner--2uFaL
computer.howstuffworks.com###cmpBackdrop
monetizepros.com##.thrv-leads-slide-in.tve_no_drag.tve_no_icons.thrv_wrapper.tve_editor_main_content.tve_empty_dropzone
monetizepros.com##div.textwidget custom-html-widget > div > a
pri.org###\31 81113
##.js-cta_prompt_item.cta_message.cta_message--load_under.GoogleAnalyticsET-processed
pcmag.com###_evidon-background
pcmag.com##.evidon-background
pcmag.com###_evidon-banner
pcmag.com##.evidon-barrier
pcmag.com###_evidon-barrier-wrapper
amazon.com##.nav-signin-tt.nav-flyout
amazon.com##.a-section.glow-toaster.glow-toaster-theme-default.glow-toaster-slot-default.nav-coreFlyout.nav-flyout
youtube.com##paper-dialog.style-scope.ytd-consent-bump-lightbox
youtube.com##paper-dialog.style-scope.ytd-popup-container
youtube.com##.style-scope.yt-upsell-dialog-renderer
||gstatic.com/youtube/img/promos/growth/dmod_si_horizontal_ver1_240x400.png
youtube.com###dialog
youtube.com##.opened
blog.hubspot.com##.show
blog.hubspot.com###slidebox
downloads.digitaltrends.com###CybotCookiebotDialogBodyUnderlay
lepapier.e-monsite.com###ems-try-link
lepapier.e-monsite.com###ems-try-close
superprof.fr##.cta-online__video-wrapper
superprof.fr###cta-online
superprof.fr##.cta-online
linuxconfig.org##.size-45.hidden-phone.g-block
alternativeto.net##.iubenda-cs-content
##.roof-ad
###roof-ad
##.ad-layer
